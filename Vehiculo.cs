﻿using System.IO;

namespace Problema3
{
    class Vehiculo
    {
        #region Atributos
        string matricula;
        string marca;
        string modelo;
        string color;
        const int TAM_MATRICULA = 15;
        const int TAM_MARCA = 20;
        const int TAM_MODELO = 40;
        const int TAM_COLOR = 15;
        FileStream fichero = Program.archivo;
        BinaryReader lector = Program.lector;
        BinaryWriter escritor = Program.escritor;
        #endregion

        #region Constructores
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public Vehiculo()
        {

        }

        /// <summary>
        /// Constructor para crear un vehiculo
        /// </summary>
        /// <param name="matricula"></param>
        /// <param name="marca"></param>
        /// <param name="modelo"></param>
        /// <param name="color"></param>
        public Vehiculo(string matricula, string marca, string modelo, string color)
        {
            this.matricula = matricula;
            this.marca = marca;
            this.modelo = modelo;
            this.color = color;
        }
        #endregion

        #region Metodos
        /// <summary>
        /// Lee el siguiente vehiculo
        /// </summary>
        public void Leer()
        {
            long posicionInicial = fichero.Position;
            matricula = lector.ReadString();
            marca = lector.ReadString();
            modelo = lector.ReadString();
            color = lector.ReadString();
            long posicionFinal = fichero.Position;
            int diferencia = (int)(posicionFinal - posicionInicial);
            fichero.Position += TamRegistro - diferencia;
        }

        /// <summary>
        /// Escribe el vehiculo
        /// </summary>
        /// <param name="ultimo"></param>
        public void Escribir(bool ultimo)
        {
            if (ultimo)
            {
                fichero.Seek(0, SeekOrigin.End);
            }
            long posicionInicial = fichero.Position;
            escritor.Write(matricula);
            escritor.Write(marca);
            escritor.Write(modelo);
            escritor.Write(color);
            long posicionFinal = fichero.Position;
            int diferencia = (int)(posicionFinal - posicionInicial);
            int relleno = TamRegistro - diferencia;
            byte[] buffer = new byte[relleno];
            for (int i=0;i<buffer.Length;i++)
            {
                buffer[i] = (byte)' ';
            }
            fichero.Write(buffer, 0, relleno);
            if (ultimo)
            {
                fichero.Seek(posicionInicial, SeekOrigin.Begin);
            }
        }

        /// <summary>
        /// Situa el puntero del stream en el registro indicado
        /// </summary>
        /// <param name="posicion">registro</param>
        public void SituarPuntero(int posicion)
        {
            fichero.Seek(posicion*TamRegistro, SeekOrigin.Begin);
        }

        /// <summary>
        /// Vuelve a leer el registro actual
        /// </summary>
        public void Releer()
        {
            fichero.Seek(-TamRegistro, SeekOrigin.Current);
            Leer();
        }

        /// <summary>
        /// Elimina el vehiculo
        /// </summary>
        public void Eliminar()
        {
            long posicionAEscribir = fichero.Position - TamRegistro;
            long posicionACopiar = fichero.Position;
            int registrosAMover = (int)(fichero.Length - fichero.Position) / TamRegistro;
            if(registrosAMover>0)
            {
                for (int i = 0; i < registrosAMover; i++)
                {
                    fichero.Seek(posicionACopiar, SeekOrigin.Begin);
                    Leer();
                    fichero.Seek(posicionAEscribir, SeekOrigin.Begin);
                    Escribir(false);
                    posicionAEscribir += TamRegistro;
                    posicionACopiar += TamRegistro;
                }
            }
            fichero.SetLength(fichero.Length - TamRegistro);
        }

        /// <summary>
        /// Edita el vehiculo
        /// </summary>
        /// <param name="matricula"></param>
        /// <param name="marca"></param>
        /// <param name="modelo"></param>
        /// <param name="color"></param>
        public void Editar(string matricula, string marca, string modelo, string color)
        {
            fichero.Seek(-TamRegistro, SeekOrigin.Current);
            Leer();
            fichero.Seek(-TamRegistro, SeekOrigin.Current);
            this.matricula = matricula;
            this.marca = marca;
            this.modelo = modelo;
            this.color = color;
            Escribir(false);
        }
        #endregion

        #region Propiedades
        /// <summary>
        /// Tamaño de cada registro
        /// </summary>
        public int TamRegistro
        {
            get
            {
                return (TAM_MATRICULA * 2) + (TAM_MODELO * 2) + (TAM_MARCA * 2) + (TAM_COLOR * 2);
            }
        }

        /// <summary>
        /// Total de registros en el fichero
        /// </summary>
        public int TotalRegistros
        {
            get
            {
                return (int)fichero.Length / TamRegistro; ;
            }
        }

        /// <summary>
        /// Devuelve la matrícula
        /// </summary>
        public string Matricula
        {
            get
            {
                return matricula;
            }
        }

        /// <summary>
        /// Devuelve la marca
        /// </summary>
        public string Marca
        {
            get
            {
                return marca;
            }
        }

        /// <summary>
        /// Devuelve el modelo
        /// </summary>
        public string Modelo
        {
            get
            {
                return modelo;
            }
        }

        /// <summary>
        /// Devuelve el color
        /// </summary>
        public string Color
        {
            get
            {
                return color;
            }
        }
        #endregion
    }
}
