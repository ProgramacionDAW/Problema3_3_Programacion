﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;

namespace Problema3
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        static public FileStream archivo;
        static public BinaryReader lector;
        static public BinaryWriter escritor;
        static public List<Vehiculo> vehiculos = new List<Vehiculo>();
    }
}
