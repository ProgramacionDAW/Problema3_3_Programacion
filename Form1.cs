﻿using System;
using System.Windows.Forms;
using System.IO;

namespace Problema3
{
    public partial class Form1 : Form
    {
        FileStream fichero;
        BinaryReader lector;
        BinaryWriter escritor;
        StreamWriter informe;
        int totalRegistros;
        int registroActualTemp;

        /// <summary>
        /// Constructor base
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            AbrirStream();
            textMarca.Location = comboMarca.Location; //Superponemos los controles
            fichero = Program.archivo;
            lector = Program.lector;
            escritor = Program.escritor;
            AgregarMarcas();
            Vehiculo vehiculo = new Vehiculo();
            totalRegistros = vehiculo.TotalRegistros; //Obtenemos el total de vehiculos
            if (totalRegistros > 0)
            {
                labelTotal.Text = string.Concat("de ", @"{", totalRegistros, @"}");
                LeerPrimerVehiculo();
            }
        }

        /// <summary>
        /// Asigna un valor al stream y crea el archivo en caso de que no exista
        /// </summary>
        private void AbrirStream()
        {
            bool nuevo = false;
            try
            {
                Program.archivo = new FileStream(@"VEHICULOS.fich", FileMode.Open, FileAccess.ReadWrite);
            }
            catch(FileNotFoundException) //Si no existe el archivo...
            {
                Program.archivo = new FileStream(@"VEHICULOS.fich", FileMode.CreateNew, FileAccess.ReadWrite);
                nuevo = true;
                MessageBox.Show("No se encontró ningun fichero con datos. Se ha generado uno nuevo.", "Error al cargar datos", MessageBoxButtons.OK);
            }
            Program.escritor = new BinaryWriter(Program.archivo);
            Program.lector = new BinaryReader(Program.archivo);
            if (nuevo) //si es nuevo, se agregan 10 vehiculos de ejemplo
            {
                VehiculosEjemplo();
            }
        }

        /// <summary>
        /// Agrega vehiculos de ejemplo al archivo
        /// </summary>
        private void VehiculosEjemplo()
        {
            string[] matriculas = new string[10] { "8165DYL", "4367YVZ", "9646ABC", "9647YGH", "1206LNM", "8564KHG", "6472NBC", "8474KDA", "6587YTU", "9867HJK" };
            string[] marcas = new string[10] { "Ford", "Renault", "Audi", "BMW", "Chevrolet", "Citroen", "Fiat", "Audi", "BMW", "Chevrolet" };
            string[] modelos = new string[10] { "Kuga", "Coupé", "A3", "320D", "Camaro", "C4", "Punto", "S8", "330e", "Cruze" };
            string[] colores = new string[10] { "Negro", "Azul", "Blanco", "Perla", "Amarillo", "Gris", "Azul", "Negro", "Gris", "Rojo" };
            for (int i=0;i<10;i++)
            {
                Vehiculo vehiculo = new Vehiculo(matriculas[i], marcas[i], modelos[i], colores[i]);
                vehiculo.Escribir(true);
            }
            Program.archivo.Seek(0, SeekOrigin.Begin);
        }

        /// <summary>
        /// Cierra todos los streams
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Program.lector.Close();
            Program.escritor.Close();
            Program.archivo.Close();
        }

        /// <summary>
        /// Cierra el formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void opcionSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Guarda y escribe el vehiculo en el archivo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonGuardar_Click(object sender, EventArgs e)
        {
            int registroActual = int.Parse(labelActual.Text);
            if(string.IsNullOrWhiteSpace(textMatricula.Text))
            {
                MessageBox.Show("La matrícula no puede estar vacía", "Error", MessageBoxButtons.OK);
                return;
            }
            textMatricula.Text = textMatricula.Text.ToUpper();
            Vehiculo vehiculo = new Vehiculo(textMatricula.Text, comboMarca.Text, textModelo.Text, textColor.Text);
            vehiculo.Escribir(true);

            botonGuardar.Visible = false;
            textMarca.Visible = true;
            comboMarca.Visible = false;
            textMatricula.ReadOnly = true;
            textModelo.ReadOnly = true;
            textColor.ReadOnly = true;
            textMatricula.Clear();
            textModelo.Clear();
            textColor.Clear();
            textMarca.Clear();
            HabilitarPanel2();
            labelTotal.Text = string.Concat("de ", @"{", vehiculo.TotalRegistros, @"}");
            totalRegistros++;

            if (registroActual == 0)
            {
                fichero.Seek(0, SeekOrigin.Begin);
                textMatricula.Clear();
                textMarca.Clear();
                textModelo.Clear();
                textColor.Clear();
            }
            else //lo movemos al registro anterior
            {
                vehiculo.SituarPuntero(registroActual - 1);
                vehiculo.Leer();
                textMatricula.Text = vehiculo.Matricula;
                textMarca.Text = vehiculo.Marca;
                textModelo.Text = vehiculo.Modelo;
                textColor.Text = vehiculo.Color;
            }
        }

        /// <summary>
        /// Agrega marcas al comboBox
        /// </summary>
        private void AgregarMarcas()
        {
            string[] marcas = new string[] { "Ford", "Renault", "Audi", "BMW", "Chevrolet", "Citroen", "Fiat" };
            Array.Sort(marcas);
            for (int i=0;i<marcas.Length;i++)
            {
                comboMarca.Items.Add(marcas[i]);
            }
        }

        /// <summary>
        /// Prepara el formulario para agregar un vehiculo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonAgregar_Click(object sender, EventArgs e)
        {
            textMarca.Visible = false;
            comboMarca.Visible = true;
            botonGuardar.Visible = true;
            DeshabilitarPanel();
            textMatricula.Clear();
            textModelo.Clear();
            textColor.Clear();
            textMarca.Clear();
            textMatricula.ReadOnly = false;
            textModelo.ReadOnly = false;
            textColor.ReadOnly = false;
        }

        /// <summary>
        /// Lee el primer vehiculo del archivo y lo muestra
        /// </summary>
        private void LeerPrimerVehiculo()
        {
            Vehiculo vehiculo = new Vehiculo();
            vehiculo.Leer();
            textMatricula.Text = vehiculo.Matricula;
            textMarca.Text = vehiculo.Marca;
            textModelo.Text = vehiculo.Modelo;
            textColor.Text = vehiculo.Color;
            labelActual.Text = "1";
        }

        /// <summary>
        /// Lee el siguiente vehiculo y lo muestra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonSiguiente_Click(object sender, EventArgs e)
        {
            int registroActual = int.Parse(labelActual.Text);
            if (registroActual < totalRegistros)
            {
                Vehiculo vehiculo = new Vehiculo();
                vehiculo.Leer();
                textMatricula.Text = vehiculo.Matricula;
                textMarca.Text = vehiculo.Marca;
                textModelo.Text = vehiculo.Modelo;
                textColor.Text = vehiculo.Color;
                labelActual.Text = (registroActual + 1).ToString();
            }
        }

        /// <summary>
        /// Lee el vehiculo anterior y lo muestra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonAnterior_Click(object sender, EventArgs e)
        {
            int registroActual = int.Parse(labelActual.Text);
            if (registroActual > 1) 
            {
                Vehiculo vehiculo = new Vehiculo();
                vehiculo.SituarPuntero(registroActual-2);
                vehiculo.Leer();
                textMatricula.Text = vehiculo.Matricula;
                textMarca.Text = vehiculo.Marca;
                textModelo.Text = vehiculo.Modelo;
                textColor.Text = vehiculo.Color;
                labelActual.Text = (registroActual - 1).ToString();
            }
        }

        /// <summary>
        /// Lee el primer vehiculo y lo muestra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonPrimero_Click(object sender, EventArgs e)
        {
            int registroActual = int.Parse(labelActual.Text);
            if (registroActual > 1)
            {
                Vehiculo vehiculo = new Vehiculo();
                vehiculo.SituarPuntero(0);
                vehiculo.Leer();
                textMatricula.Text = vehiculo.Matricula;
                textMarca.Text = vehiculo.Marca;
                textModelo.Text = vehiculo.Modelo;
                textColor.Text = vehiculo.Color;
                labelActual.Text = "1";
            }
        }

        /// <summary>
        /// Lee el ultimo vehiculo y lo muestra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonUltimo_Click(object sender, EventArgs e)
        {
            int registroActual = int.Parse(labelActual.Text);
            if (registroActual < totalRegistros)
            {
                Vehiculo vehiculo = new Vehiculo();
                vehiculo.SituarPuntero(totalRegistros-1);
                vehiculo.Leer();
                textMatricula.Text = vehiculo.Matricula;
                textMarca.Text = vehiculo.Marca;
                textModelo.Text = vehiculo.Modelo;
                textColor.Text = vehiculo.Color;
                labelActual.Text = totalRegistros.ToString();
            }
        }


        /// <summary>
        /// Lee el vehiculo seleccionado y lo muestra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labelActual_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar==(char)Keys.Enter)
            {
                int registro;
                if (!int.TryParse(labelActual.Text, out registro))
                {
                    MessageBox.Show("Dato introducido no valido", "Error", MessageBoxButtons.OK);
                    labelActual.Text = registroActualTemp.ToString();
                }
                else if (registro<=0)
                {
                    MessageBox.Show("Dato introducido no valido", "Error", MessageBoxButtons.OK);
                    labelActual.Text = registroActualTemp.ToString();
                }
                else
                {
                    if (registro <= totalRegistros)
                    {
                        Vehiculo vehiculo = new Vehiculo();
                        vehiculo.SituarPuntero(registro - 1);
                        vehiculo.Leer();
                        textMatricula.Text = vehiculo.Matricula;
                        textMarca.Text = vehiculo.Marca;
                        textModelo.Text = vehiculo.Modelo;
                        textColor.Text = vehiculo.Color;
                        labelActual.Text = registro.ToString();
                        registroActualTemp = registro;
                    }
                    else
                    {
                        MessageBox.Show("Registro introducido no valido", "Error", MessageBoxButtons.OK);
                        labelActual.Text = registroActualTemp.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// Deshabilita ciertos controles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labelActual_Enter(object sender, EventArgs e)
        {
            botonPrimero.Enabled = false;
            botonUltimo.Enabled = false;
            botonSiguiente.Enabled = false;
            botonAnterior.Enabled = false;
            botonAgregar.Enabled = false;
            botonEliminar.Enabled = false;
            botonEditar.Enabled = false;
            registroActualTemp = int.Parse(labelActual.Text);
        }

        /// <summary>
        /// Habilita ciertos controles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labelActual_Leave(object sender, EventArgs e)
        {
            botonAnterior.Enabled = true;
            botonSiguiente.Enabled = true;
            botonUltimo.Enabled = true;
            botonPrimero.Enabled = true;
            botonAgregar.Enabled = true;
            botonEliminar.Enabled = true;
            botonEditar.Enabled = true;
            int registro;
            if (!int.TryParse(labelActual.Text, out registro))
            {
                labelActual.Text = registroActualTemp.ToString();
            }
            if (registro != registroActualTemp)
            {
                labelActual.Text = registroActualTemp.ToString();
            }
        }

        /// <summary>
        /// Deshabilita controles del formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonEditar_Click(object sender, EventArgs e)
        {
            int registroActual = int.Parse(labelActual.Text);
            if (registroActual>0)
            {
                textMarca.Visible = false;
                comboMarca.Visible = true;
                comboMarca.Text = textMarca.Text;
                textMatricula.ReadOnly = false;
                textModelo.ReadOnly = false;
                textColor.ReadOnly = false;
                botonSave.Visible = true;
                botonDiscard.Visible = true;
                DeshabilitarPanel();
            }
            else
            {
                MessageBox.Show("No se puede editar este registro", "Error", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// Cancela los cambios y relee el vehiculo actual
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonDiscard_Click(object sender, EventArgs e)
        {
            Vehiculo vehiculo = new Vehiculo();
            vehiculo.Releer();
            textMatricula.Text = vehiculo.Matricula;
            textMarca.Text = vehiculo.Marca;
            textModelo.Text = vehiculo.Modelo;
            textColor.Text = vehiculo.Color;
            botonDiscard.Visible = false;
            botonSave.Visible = false;
            HabilitarPanel();
        }

        /// <summary>
        /// Guarda los cambios y edita el vehiculo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textMatricula.Text))
            {
                MessageBox.Show("La matrícula no puede estar vacía", "Error", MessageBoxButtons.OK);
                return;
            }
            Vehiculo vehiculo = new Vehiculo();
            vehiculo.Editar(textMatricula.Text, comboMarca.Text, textModelo.Text, textColor.Text);
            botonDiscard.Visible = false;
            botonSave.Visible = false;
            HabilitarPanel();
        }

        /// <summary>
        /// Deshabilita ciertos controles
        /// </summary>
        private void DeshabilitarPanel()
        {
            botonPrimero.Enabled = false;
            botonAnterior.Enabled = false;
            botonSiguiente.Enabled = false;
            botonUltimo.Enabled = false;
            botonAgregar.Enabled = false;
            botonEliminar.Enabled = false;
            botonEditar.Enabled = false;
            labelActual.Enabled = false;
            opcionBuscar.Enabled = false;
        }

        /// <summary>
        /// Habilita ciertos controles
        /// </summary>
        public void HabilitarPanel2()
        {
            botonPrimero.Enabled = true;
            botonAnterior.Enabled = true;
            botonSiguiente.Enabled = true;
            botonUltimo.Enabled = true;
            botonAgregar.Enabled = true;
            botonEliminar.Enabled = true;
            botonEditar.Enabled = true;
            labelActual.Enabled = true;
            opcionBuscar.Enabled = true;
        }

        /// <summary>
        /// Habilita ciertos controles
        /// </summary>
        private void HabilitarPanel()
        {
            botonPrimero.Enabled = true;
            botonAnterior.Enabled = true;
            botonSiguiente.Enabled = true;
            botonUltimo.Enabled = true;
            botonAgregar.Enabled = true;
            botonEliminar.Enabled = true;
            opcionBuscar.Enabled = true;
            botonEditar.Enabled = true;
            labelActual.Enabled = true;
            textMarca.Visible = true;
            comboMarca.Visible = false;
            textMarca.Text = comboMarca.Text;
            textMatricula.ReadOnly = true;
            textModelo.ReadOnly = true;
            textColor.ReadOnly = true;
            botonSave.Visible = false;
            botonDiscard.Visible = false;
        }

        /// <summary>
        /// Habilita la busqueda de matriculas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void opcionBuscar_Click(object sender, EventArgs e)
        {
            if (totalRegistros<=0)
            {
                MessageBox.Show("No existen vehiculos", "Error", MessageBoxButtons.OK);
            }
            else
            {
                string matricula;
                int registroActual = int.Parse(labelActual.Text);
                int registro = 1;
                Vehiculo vehiculo = new Vehiculo();

                BuscaMatricula form = new BuscaMatricula();
                form.ShowDialog();
                matricula = form.labelMatricula.Text;

                fichero.Seek(0, SeekOrigin.Begin);
                while (true)
                {
                    try
                    {
                        vehiculo.Leer();
                        if (vehiculo.Matricula == matricula)
                        {
                            textMatricula.Text = vehiculo.Matricula;
                            textMarca.Text = vehiculo.Marca;
                            textModelo.Text = vehiculo.Modelo;
                            textColor.Text = vehiculo.Color;
                            labelActual.Text = registro.ToString();
                            return;
                        }
                        registro++;
                    }
                    catch (EndOfStreamException)
                    {
                        break;
                    }
                }
                if (registroActual <= 0)
                {
                    registroActual = 1;
                }
                vehiculo.SituarPuntero(registroActual - 1);
                vehiculo.Leer();
                textMatricula.Text = vehiculo.Matricula;
                textMarca.Text = vehiculo.Marca;
                textModelo.Text = vehiculo.Modelo;
                textColor.Text = vehiculo.Color;
                labelActual.Text = registroActual.ToString();
                MessageBox.Show("La matrícula introducida no existe.", "Información", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// Elimina un vehiculo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonEliminar_Click(object sender, EventArgs e)
        {
            int registroActual = int.Parse(labelActual.Text);
            if (registroActual>0)
            {
                Vehiculo vehiculo = new Vehiculo();
                vehiculo.Eliminar();
                registroActual--;
                if (registroActual == 0)
                {
                    fichero.Seek(0, SeekOrigin.Begin);
                    textMatricula.Clear();
                    textMarca.Clear();
                    textModelo.Clear();
                    textColor.Clear();
                }
                else
                {
                    vehiculo.SituarPuntero(registroActual - 1);
                    vehiculo.Leer();
                    textMatricula.Text = vehiculo.Matricula;
                    textMarca.Text = vehiculo.Marca;
                    textModelo.Text = vehiculo.Modelo;
                    textColor.Text = vehiculo.Color;
                }
                labelActual.Text = registroActual.ToString();
                labelTotal.Text = string.Concat("de ", @"{", vehiculo.TotalRegistros, @"}");
                totalRegistros = vehiculo.TotalRegistros;
            }
            else
            {
                MessageBox.Show("No se puede eliminar este registro", "Error", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// Abre el dialogo de selección de archivos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void abrirInforme_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string ruta = openFileDialog1.FileName;
                    informe = new StreamWriter(ruta, false);
                    botonGuardarInfo.Enabled = true;
                }
                catch
                {
                    MessageBox.Show("El archivo seleccionado no se ha podido leer");
                }
            }
        }

        /// <summary>
        /// Guarda los datos en el archivo informe
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonGuardarInfo_Click(object sender, EventArgs e)
        {
            long posicionInicial = fichero.Position;
            fichero.Seek(0, SeekOrigin.Begin);
            int id = 0;
            TextWriter escribe = informe;
            try
            {
                escribe.WriteLine("{0} {1} {2} {3} {4}", "ID".PadRight(4), "Matrícula".PadRight(15), "Marca".PadRight(20), "Modelo".PadRight(40), "Color".PadRight(15));
                while (true)
                {
                    id++;
                    Vehiculo vehiculo = new Vehiculo();
                    vehiculo.Leer();
                    escribe.WriteLine("{0} {1} {2} {3} {4}", id.ToString().PadRight(4), vehiculo.Matricula.PadRight(15), vehiculo.Marca.PadRight(20), vehiculo.Modelo.PadRight(40), vehiculo.Color.PadRight(15));
                }
            }
            catch (EndOfStreamException)
            {
                fichero.Seek(posicionInicial, SeekOrigin.Begin);
            }
            informe.Close();
            botonGuardarInfo.Enabled = false;
        }
    }
}
