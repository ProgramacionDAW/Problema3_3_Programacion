﻿namespace Problema3
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuNavegacion = new System.Windows.Forms.ToolStrip();
            this.botonPrimero = new System.Windows.Forms.ToolStripButton();
            this.botonAnterior = new System.Windows.Forms.ToolStripButton();
            this.labelActual = new System.Windows.Forms.ToolStripTextBox();
            this.labelTotal = new System.Windows.Forms.ToolStripLabel();
            this.botonSiguiente = new System.Windows.Forms.ToolStripButton();
            this.botonUltimo = new System.Windows.Forms.ToolStripButton();
            this.botonAgregar = new System.Windows.Forms.ToolStripButton();
            this.botonEliminar = new System.Windows.Forms.ToolStripButton();
            this.botonEditar = new System.Windows.Forms.ToolStripButton();
            this.botonSave = new System.Windows.Forms.ToolStripButton();
            this.botonDiscard = new System.Windows.Forms.ToolStripButton();
            this.menuPrincipal = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirInforme = new System.Windows.Forms.ToolStripMenuItem();
            this.botonGuardarInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.opcionSalir = new System.Windows.Forms.ToolStripMenuItem();
            this.herramientasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opcionBuscar = new System.Windows.Forms.ToolStripMenuItem();
            this.labelMatricula = new System.Windows.Forms.Label();
            this.labelMarca = new System.Windows.Forms.Label();
            this.labelModelo = new System.Windows.Forms.Label();
            this.labelColor = new System.Windows.Forms.Label();
            this.textMatricula = new System.Windows.Forms.TextBox();
            this.textModelo = new System.Windows.Forms.TextBox();
            this.textColor = new System.Windows.Forms.TextBox();
            this.comboMarca = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.botonGuardar = new System.Windows.Forms.Button();
            this.botonSalir = new System.Windows.Forms.Button();
            this.textMarca = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuNavegacion.SuspendLayout();
            this.menuPrincipal.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuNavegacion
            // 
            this.menuNavegacion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.menuNavegacion.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.botonPrimero,
            this.botonAnterior,
            this.labelActual,
            this.labelTotal,
            this.botonSiguiente,
            this.botonUltimo,
            this.botonAgregar,
            this.botonEliminar,
            this.botonEditar,
            this.botonSave,
            this.botonDiscard});
            this.menuNavegacion.Location = new System.Drawing.Point(0, 24);
            this.menuNavegacion.Name = "menuNavegacion";
            this.menuNavegacion.Size = new System.Drawing.Size(351, 25);
            this.menuNavegacion.TabIndex = 0;
            this.menuNavegacion.Text = "toolStrip1";
            // 
            // botonPrimero
            // 
            this.botonPrimero.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.botonPrimero.Image = ((System.Drawing.Image)(resources.GetObject("botonPrimero.Image")));
            this.botonPrimero.Name = "botonPrimero";
            this.botonPrimero.RightToLeftAutoMirrorImage = true;
            this.botonPrimero.Size = new System.Drawing.Size(23, 22);
            this.botonPrimero.Text = "Mover primero";
            this.botonPrimero.Click += new System.EventHandler(this.botonPrimero_Click);
            // 
            // botonAnterior
            // 
            this.botonAnterior.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.botonAnterior.Image = ((System.Drawing.Image)(resources.GetObject("botonAnterior.Image")));
            this.botonAnterior.Name = "botonAnterior";
            this.botonAnterior.RightToLeftAutoMirrorImage = true;
            this.botonAnterior.Size = new System.Drawing.Size(23, 22);
            this.botonAnterior.Text = "Mover anterior";
            this.botonAnterior.Click += new System.EventHandler(this.botonAnterior_Click);
            // 
            // labelActual
            // 
            this.labelActual.AccessibleName = "Posición";
            this.labelActual.AutoSize = false;
            this.labelActual.Name = "labelActual";
            this.labelActual.Size = new System.Drawing.Size(50, 23);
            this.labelActual.Text = "0";
            this.labelActual.ToolTipText = "Posición actual";
            this.labelActual.Enter += new System.EventHandler(this.labelActual_Enter);
            this.labelActual.Leave += new System.EventHandler(this.labelActual_Leave);
            this.labelActual.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.labelActual_KeyPress);
            // 
            // labelTotal
            // 
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(37, 22);
            this.labelTotal.Text = "de {0}";
            this.labelTotal.ToolTipText = "Número total de elementos";
            // 
            // botonSiguiente
            // 
            this.botonSiguiente.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.botonSiguiente.Image = ((System.Drawing.Image)(resources.GetObject("botonSiguiente.Image")));
            this.botonSiguiente.Name = "botonSiguiente";
            this.botonSiguiente.RightToLeftAutoMirrorImage = true;
            this.botonSiguiente.Size = new System.Drawing.Size(23, 22);
            this.botonSiguiente.Text = "Mover siguiente";
            this.botonSiguiente.Click += new System.EventHandler(this.botonSiguiente_Click);
            // 
            // botonUltimo
            // 
            this.botonUltimo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.botonUltimo.Image = ((System.Drawing.Image)(resources.GetObject("botonUltimo.Image")));
            this.botonUltimo.Name = "botonUltimo";
            this.botonUltimo.RightToLeftAutoMirrorImage = true;
            this.botonUltimo.Size = new System.Drawing.Size(23, 22);
            this.botonUltimo.Text = "Mover último";
            this.botonUltimo.Click += new System.EventHandler(this.botonUltimo_Click);
            // 
            // botonAgregar
            // 
            this.botonAgregar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.botonAgregar.Image = ((System.Drawing.Image)(resources.GetObject("botonAgregar.Image")));
            this.botonAgregar.Name = "botonAgregar";
            this.botonAgregar.RightToLeftAutoMirrorImage = true;
            this.botonAgregar.Size = new System.Drawing.Size(23, 22);
            this.botonAgregar.Text = "Agregar nuevo";
            this.botonAgregar.Click += new System.EventHandler(this.botonAgregar_Click);
            // 
            // botonEliminar
            // 
            this.botonEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.botonEliminar.Image = ((System.Drawing.Image)(resources.GetObject("botonEliminar.Image")));
            this.botonEliminar.Name = "botonEliminar";
            this.botonEliminar.RightToLeftAutoMirrorImage = true;
            this.botonEliminar.Size = new System.Drawing.Size(23, 22);
            this.botonEliminar.Text = "Eliminar";
            this.botonEliminar.Click += new System.EventHandler(this.botonEliminar_Click);
            // 
            // botonEditar
            // 
            this.botonEditar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.botonEditar.Image = global::Problema3.Properties.Resources.iconoEditar;
            this.botonEditar.Name = "botonEditar";
            this.botonEditar.RightToLeftAutoMirrorImage = true;
            this.botonEditar.Size = new System.Drawing.Size(23, 22);
            this.botonEditar.Text = "Editar";
            this.botonEditar.Click += new System.EventHandler(this.botonEditar_Click);
            // 
            // botonSave
            // 
            this.botonSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.botonSave.Image = global::Problema3.Properties.Resources.tick;
            this.botonSave.Name = "botonSave";
            this.botonSave.RightToLeftAutoMirrorImage = true;
            this.botonSave.Size = new System.Drawing.Size(23, 22);
            this.botonSave.Text = "Guardar cambios";
            this.botonSave.Visible = false;
            this.botonSave.Click += new System.EventHandler(this.botonSave_Click);
            // 
            // botonDiscard
            // 
            this.botonDiscard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.botonDiscard.Image = global::Problema3.Properties.Resources.cruz;
            this.botonDiscard.Name = "botonDiscard";
            this.botonDiscard.RightToLeftAutoMirrorImage = true;
            this.botonDiscard.Size = new System.Drawing.Size(23, 22);
            this.botonDiscard.Text = "Descartar cambios";
            this.botonDiscard.Visible = false;
            this.botonDiscard.Click += new System.EventHandler(this.botonDiscard_Click);
            // 
            // menuPrincipal
            // 
            this.menuPrincipal.BackColor = System.Drawing.Color.Silver;
            this.menuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.herramientasToolStripMenuItem});
            this.menuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.menuPrincipal.Name = "menuPrincipal";
            this.menuPrincipal.Size = new System.Drawing.Size(351, 24);
            this.menuPrincipal.TabIndex = 1;
            this.menuPrincipal.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirInforme,
            this.botonGuardarInfo,
            this.toolStripSeparator2,
            this.opcionSalir});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "&Archivo";
            // 
            // abrirInforme
            // 
            this.abrirInforme.Image = ((System.Drawing.Image)(resources.GetObject("abrirInforme.Image")));
            this.abrirInforme.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.abrirInforme.Name = "abrirInforme";
            this.abrirInforme.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.abrirInforme.Size = new System.Drawing.Size(201, 22);
            this.abrirInforme.Text = "&Abrir...";
            this.abrirInforme.Click += new System.EventHandler(this.abrirInforme_Click);
            // 
            // botonGuardarInfo
            // 
            this.botonGuardarInfo.Enabled = false;
            this.botonGuardarInfo.Image = ((System.Drawing.Image)(resources.GetObject("botonGuardarInfo.Image")));
            this.botonGuardarInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.botonGuardarInfo.Name = "botonGuardarInfo";
            this.botonGuardarInfo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.botonGuardarInfo.Size = new System.Drawing.Size(201, 22);
            this.botonGuardarInfo.Text = "&Guardar Informe";
            this.botonGuardarInfo.Click += new System.EventHandler(this.botonGuardarInfo_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(198, 6);
            // 
            // opcionSalir
            // 
            this.opcionSalir.Name = "opcionSalir";
            this.opcionSalir.Size = new System.Drawing.Size(201, 22);
            this.opcionSalir.Text = "&Salir";
            this.opcionSalir.Click += new System.EventHandler(this.opcionSalir_Click);
            // 
            // herramientasToolStripMenuItem
            // 
            this.herramientasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.opcionBuscar});
            this.herramientasToolStripMenuItem.Name = "herramientasToolStripMenuItem";
            this.herramientasToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.herramientasToolStripMenuItem.Text = "&Buscar";
            // 
            // opcionBuscar
            // 
            this.opcionBuscar.Name = "opcionBuscar";
            this.opcionBuscar.Size = new System.Drawing.Size(133, 22);
            this.opcionBuscar.Text = "&Matrícula...";
            this.opcionBuscar.ToolTipText = "Buscar matrícula";
            this.opcionBuscar.Click += new System.EventHandler(this.opcionBuscar_Click);
            // 
            // labelMatricula
            // 
            this.labelMatricula.AutoSize = true;
            this.labelMatricula.BackColor = System.Drawing.Color.Transparent;
            this.labelMatricula.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMatricula.Location = new System.Drawing.Point(20, 74);
            this.labelMatricula.Name = "labelMatricula";
            this.labelMatricula.Size = new System.Drawing.Size(85, 24);
            this.labelMatricula.TabIndex = 2;
            this.labelMatricula.Text = "Matrícula";
            // 
            // labelMarca
            // 
            this.labelMarca.AutoSize = true;
            this.labelMarca.BackColor = System.Drawing.Color.Transparent;
            this.labelMarca.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMarca.Location = new System.Drawing.Point(43, 114);
            this.labelMarca.Name = "labelMarca";
            this.labelMarca.Size = new System.Drawing.Size(62, 24);
            this.labelMarca.TabIndex = 3;
            this.labelMarca.Text = "Marca";
            // 
            // labelModelo
            // 
            this.labelModelo.AutoSize = true;
            this.labelModelo.BackColor = System.Drawing.Color.Transparent;
            this.labelModelo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModelo.Location = new System.Drawing.Point(31, 154);
            this.labelModelo.Name = "labelModelo";
            this.labelModelo.Size = new System.Drawing.Size(74, 24);
            this.labelModelo.TabIndex = 4;
            this.labelModelo.Text = "Modelo";
            // 
            // labelColor
            // 
            this.labelColor.AutoSize = true;
            this.labelColor.BackColor = System.Drawing.Color.Transparent;
            this.labelColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelColor.Location = new System.Drawing.Point(50, 194);
            this.labelColor.Name = "labelColor";
            this.labelColor.Size = new System.Drawing.Size(55, 24);
            this.labelColor.TabIndex = 5;
            this.labelColor.Text = "Color";
            // 
            // textMatricula
            // 
            this.textMatricula.Location = new System.Drawing.Point(124, 78);
            this.textMatricula.MaxLength = 15;
            this.textMatricula.Name = "textMatricula";
            this.textMatricula.ReadOnly = true;
            this.textMatricula.Size = new System.Drawing.Size(129, 20);
            this.textMatricula.TabIndex = 6;
            // 
            // textModelo
            // 
            this.textModelo.Location = new System.Drawing.Point(124, 158);
            this.textModelo.MaxLength = 40;
            this.textModelo.Name = "textModelo";
            this.textModelo.ReadOnly = true;
            this.textModelo.Size = new System.Drawing.Size(129, 20);
            this.textModelo.TabIndex = 7;
            // 
            // textColor
            // 
            this.textColor.Location = new System.Drawing.Point(124, 198);
            this.textColor.MaxLength = 15;
            this.textColor.Name = "textColor";
            this.textColor.ReadOnly = true;
            this.textColor.Size = new System.Drawing.Size(129, 20);
            this.textColor.TabIndex = 8;
            // 
            // comboMarca
            // 
            this.comboMarca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMarca.FormattingEnabled = true;
            this.comboMarca.Location = new System.Drawing.Point(124, 116);
            this.comboMarca.MaxLength = 20;
            this.comboMarca.Name = "comboMarca";
            this.comboMarca.Size = new System.Drawing.Size(129, 21);
            this.comboMarca.TabIndex = 9;
            this.comboMarca.Visible = false;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Location = new System.Drawing.Point(-36, 240);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(500, 2);
            this.label5.TabIndex = 10;
            // 
            // botonGuardar
            // 
            this.botonGuardar.Location = new System.Drawing.Point(43, 260);
            this.botonGuardar.Name = "botonGuardar";
            this.botonGuardar.Size = new System.Drawing.Size(75, 23);
            this.botonGuardar.TabIndex = 11;
            this.botonGuardar.Text = "Guardar";
            this.botonGuardar.UseVisualStyleBackColor = true;
            this.botonGuardar.Visible = false;
            this.botonGuardar.Click += new System.EventHandler(this.botonGuardar_Click);
            // 
            // botonSalir
            // 
            this.botonSalir.Location = new System.Drawing.Point(224, 260);
            this.botonSalir.Name = "botonSalir";
            this.botonSalir.Size = new System.Drawing.Size(75, 23);
            this.botonSalir.TabIndex = 12;
            this.botonSalir.Text = "Salir";
            this.botonSalir.UseVisualStyleBackColor = true;
            this.botonSalir.Click += new System.EventHandler(this.opcionSalir_Click);
            // 
            // textMarca
            // 
            this.textMarca.Location = new System.Drawing.Point(214, 118);
            this.textMarca.Name = "textMarca";
            this.textMarca.ReadOnly = true;
            this.textMarca.Size = new System.Drawing.Size(129, 20);
            this.textMarca.TabIndex = 13;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "INFORME";
            this.openFileDialog1.Filter = "txt files (*.txt)|*.txt";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(351, 299);
            this.Controls.Add(this.textMarca);
            this.Controls.Add(this.botonSalir);
            this.Controls.Add(this.botonGuardar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboMarca);
            this.Controls.Add(this.textColor);
            this.Controls.Add(this.textModelo);
            this.Controls.Add(this.textMatricula);
            this.Controls.Add(this.labelColor);
            this.Controls.Add(this.labelModelo);
            this.Controls.Add(this.labelMarca);
            this.Controls.Add(this.labelMatricula);
            this.Controls.Add(this.menuNavegacion);
            this.Controls.Add(this.menuPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuPrincipal;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Problema 3";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.menuNavegacion.ResumeLayout(false);
            this.menuNavegacion.PerformLayout();
            this.menuPrincipal.ResumeLayout(false);
            this.menuPrincipal.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip menuNavegacion;
        private System.Windows.Forms.ToolStripButton botonPrimero;
        private System.Windows.Forms.ToolStripButton botonAnterior;
        private System.Windows.Forms.ToolStripTextBox labelActual;
        private System.Windows.Forms.ToolStripLabel labelTotal;
        private System.Windows.Forms.ToolStripButton botonSiguiente;
        private System.Windows.Forms.ToolStripButton botonUltimo;
        private System.Windows.Forms.ToolStripButton botonAgregar;
        private System.Windows.Forms.ToolStripButton botonEliminar;
        private System.Windows.Forms.MenuStrip menuPrincipal;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirInforme;
        private System.Windows.Forms.ToolStripMenuItem botonGuardarInfo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem opcionSalir;
        private System.Windows.Forms.ToolStripMenuItem herramientasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opcionBuscar;
        private System.Windows.Forms.Label labelMatricula;
        private System.Windows.Forms.Label labelMarca;
        private System.Windows.Forms.Label labelModelo;
        private System.Windows.Forms.Label labelColor;
        private System.Windows.Forms.TextBox textMatricula;
        private System.Windows.Forms.TextBox textModelo;
        private System.Windows.Forms.TextBox textColor;
        private System.Windows.Forms.ComboBox comboMarca;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button botonGuardar;
        private System.Windows.Forms.Button botonSalir;
        private System.Windows.Forms.TextBox textMarca;
        private System.Windows.Forms.ToolStripButton botonEditar;
        private System.Windows.Forms.ToolStripButton botonSave;
        private System.Windows.Forms.ToolStripButton botonDiscard;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

